
const Home = {
    text: 'Home',
    link: '/home',
    icon: 'icon-home'
};

const headingMain = {
    text: 'Main Navigation',
    heading: true
};
const Users = {
    text: 'User Management',
    link: '/users',
    icon: 'icon-user'
};

export const menu = [
    headingMain,
    Home,
    Users
];
