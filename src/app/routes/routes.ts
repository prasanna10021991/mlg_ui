import { LayoutComponent } from '../layout/layout.component';
import { AuthGuard } from '../services/auth.guard';
import { LoginComponent } from './login/login.component';

export const routes = [

    { path: 'login', component: LoginComponent},
    {
        path: '',
        component: LayoutComponent,
        canActivateChild: [AuthGuard],
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' },
            { path: 'users', loadChildren: './user-management/user-management.module#UserManagementModule' }
        ]
    },
    { path: '**', redirectTo: 'home', pathMatch: 'full',canActivate: [AuthGuard] }
];
