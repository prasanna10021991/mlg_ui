import { NgModule } from '@angular/core';
import { UserManagementComponent } from './user-management/user-management.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: UserManagementComponent },
];
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  declarations: [UserManagementComponent],
  exports: [
    RouterModule
  ]
})
export class UserManagementModule { }
